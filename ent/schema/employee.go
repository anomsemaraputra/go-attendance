package schema

import (
	"time"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
)

// Employee holds the schema definition for the Employee entity.
type Employee struct {
	ent.Schema
}

// Fields of the Employee.
func (Employee) Fields() []ent.Field {
	return []ent.Field{
		field.String("name").MaxLen(200),
		field.String("email").MaxLen(200).Unique(),
		field.String("password").NotEmpty(),
		field.Uint8("role").Default(2),
		field.Time("created_at").Default(time.Now),
		field.Time("updated_at").Default(time.Now),

	}
}

// Edges of the Employee.
func (Employee) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("attendance", Attendance.Type),
	}

}
