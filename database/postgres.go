package database

import (
	"context"
	"fmt"
	"log"

	"go-attendance/ent"

	_ "github.com/lib/pq"
)
var (
	host     	= "localhost"
	user     	= "postgres1"
	password 	= "postgres1"
	port     	= 5432
	dbname		= "attendance"

)

func StartDB() {
	config := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host,port,user,password,dbname)
    client, err := ent.Open("postgres", config)
    if err != nil {
        log.Fatalf("failed opening connection to postgres: %v", err)
    }
    defer client.Close()
    // Run the auto migration tool.
    if err := client.Schema.Create(context.Background()); err != nil {
        log.Fatalf("failed creating schema resources: %v", err)
    }

}