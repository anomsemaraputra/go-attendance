package main

import (
	"go-attendance/database"
	"go-attendance/routers"
)

func main() {
	database.StartDB() 
	e := routers.Init()
	e.Logger.Fatal(e.Start(":3000"))
}