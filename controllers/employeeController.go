package controllers

import (
	"context"
	"go-attendance/ent"
	"net/http"

	"github.com/labstack/echo/v4"
)

type Controller struct{
	client *ent.Client
}

func (c *Controller) EmployeeRegister(e echo.Context) error{
	var employee ent.Employee
	// bind request to Employee struct
	if err := e.Bind(&employee); err != nil {
		e.Logger().Error("Bind: ", err)
		
		return e.String(http.StatusBadRequest, "Bind: "+err.Error())
	}
	// insert record
	cc := c.client.Employee.Create().SetName(employee.Name)
	if employee.Name != "" {
		cc.SetName(employee.Name)
	}
	newEmployee, err := cc.Save(context.Background())
	if err != nil {
		e.Logger().Error("Insert: ", err)
		
		return e.String(http.StatusBadRequest, "Save: "+err.Error())
	}
	e.Logger().Infof("inserted Employee: %v", newEmployee.ID)
	return e.NoContent(http.StatusCreated)
}